<?php

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase {


    public function testCalculAge(){
        $this->assertEquals(19, calculAge("2001-07-28"));
        $this->assertEquals(19, calculAge("2001-01-28"));
        $this->assertEquals(19, calculAge("2001-11-28"));
    }

}
?>