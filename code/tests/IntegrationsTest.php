<?php
require_once 'vendor/autoload.php';

class IntegrationsTest extends IntegrationTestCase{

    public function testIndex()
    {
        $response = $this->makeRequest("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());        
        //$this->assertEquals("Hello World!", $response->getBody()->getContents());
        //$this->assertContains("Hello World!", $response->getBody()[0]);
        //$this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
    public function testLogin()
    {
        $response = $this->makeRequest("GET", "/login");
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function testRegister()
    {
        $response = $this->makeRequest("GET", "/register");
        $this->assertEquals(200, $response->getStatusCode());
    }
    public function testProfilPost()
    {
        $response = $this->makeRequest("POST", "/profil");
        $this->assertEquals(200, $response->getStatusCode());
    } 
    public function testParametres()
    {
        $response = $this->makeRequest("GET", "/parametres");
        $this->assertEquals(200, $response->getStatusCode());
    } 
    public function testLogOut()
    {
        $response = $this->makeRequest("GET", "/parametres");
        $this->assertEquals(200, $response->getStatusCode());
    } 
}