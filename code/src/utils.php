<?php

use PHPUnit\Framework\TestCase;
use Symfony\Component\Process\Process;
use GuzzleHttp\Client;

abstract class IntegrationTestCase extends TestCase {

    private static $process;

    public static function setUpBeforeClass(): void
    {
        self::$process = new Process(["php", "-S", "localhost:808", "-t", "."]);
        self::$process->start();
        usleep(100000); //wait for server to get going
    }

    public static function tearDownAfterClass(): void
    {
        self::$process->stop();
    }

    public function getClient()
    {
        return new Client(['http_errors' => false]);
    }

    public function buildUrl($url)
    {
        return "localhost:808" . $url;
    }

    public function makeRequest($method, $url)
    {
        $client =  $this->getClient();
        return $client->request($method, $this->buildUrl($url));
    }
}