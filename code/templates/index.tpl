{*Commentaire Smarty*}
<!doctype html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>{$titre}</title>
    </head>
    <body>
        <div class="container">
            <nav class="navbar navbar-dark bg-primary">
                <a class="navbar-brand" href="."><strong>Social-Network</strong></a>
                <div class="col-6 text-right">
                {if isset($connecte)}
                    <a href="./logout" class="btn btn-primary btn-lg">Se déconnecter</a>
                {else}
                    <a href="./register" class="btn btn-primary btn-lg">S'inscrire</a>
                    <a href="./login" class="btn btn-primary btn-lg ml-3">Se connecter</a>
                {/if}
                </div>
            </nav>
            <section class="text-center mt-4">
                <h1><span class="text-primary font-weight-bold">Social-Network</span> - Accueil</h1>
                {if isset($connecte)}
                    <h5>Bienvenue {$nom}</h5>
                {else}
        		    <h5>Bonjour nouvel utilisateur</h5>
                {/if}
            </section> 
            <section class="text-center mt-5 mb-5">
                {if isset($connecte)}
                    <a href="./logout" class="btn btn-primary btn-lg ml-3">Se déconnecter</a>
                {else}
                    <a href="./register" class="btn btn-primary btn-lg">S'inscrire</a>
                    <a href="./login" class="btn btn-primary btn-lg ml-3">Se connecter</a>
                {/if}
            </section>
            <div>            
                <section class="row col-lg-12">
                    <section class="col-md-4 offset-md-1 border border-primary overflow-auto" style="height: 450px; overflow-y: scroll;">
                        <h1 class="display-5 text-center">Mes amis</h1>
                        <ul class="list-unstyled">
                            <li class="row text-center justify-content-center">
                                <img class="m-2" style="width: 70px;" alt="Image de profil" src="https://cdn-images.zoobeauval.com/JeXvZ0A8GGle5C36g86r6Cm143c=/1000x750/https://s3.eu-west-3.amazonaws.com/images.zoobeauval.com/2020/06/bn-1-5ef05b46dec6b.jpg">
                                <p class="lead align-items-center pt-2"><a class="text-dark">NOM Prénom</a></p>
                            </li>
                            <li class="row text-center justify-content-center">
                                <img class="float-left m-2" style="width: 70px;" alt="Image de profil" src="https://cdn-images.zoobeauval.com/JeXvZ0A8GGle5C36g86r6Cm143c=/1000x750/https://s3.eu-west-3.amazonaws.com/images.zoobeauval.com/2020/06/bn-1-5ef05b46dec6b.jpg">
                                <p class="lead align-items-center pt-2"><a class="text-dark">NOM Prénom</a></p>
                            </li>
                            <li class="row text-center justify-content-center">
                                <img class="float-left m-2" style="width: 70px;" alt="Image de profil" src="https://cdn-images.zoobeauval.com/JeXvZ0A8GGle5C36g86r6Cm143c=/1000x750/https://s3.eu-west-3.amazonaws.com/images.zoobeauval.com/2020/06/bn-1-5ef05b46dec6b.jpg">
                                <p class="lead align-items-center pt-2"><a class="text-dark">NOM Prénom</a></p>
                            </li>
                            <li class="row text-center justify-content-center">
                                <img class="float-left m-2" style="width: 70px;" alt="Image de profil" src="https://cdn-images.zoobeauval.com/JeXvZ0A8GGle5C36g86r6Cm143c=/1000x750/https://s3.eu-west-3.amazonaws.com/images.zoobeauval.com/2020/06/bn-1-5ef05b46dec6b.jpg">
                                <p class="lead align-items-center pt-2"><a class="text-dark">NOM Prénom</a></p>
                            </li>
                            <li class="row text-center justify-content-center">
                                <img class="float-left m-2" style="width: 70px;" alt="Image de profil" src="https://cdn-images.zoobeauval.com/JeXvZ0A8GGle5C36g86r6Cm143c=/1000x750/https://s3.eu-west-3.amazonaws.com/images.zoobeauval.com/2020/06/bn-1-5ef05b46dec6b.jpg">
                                <p class="lead align-items-center pt-2"><a class="text-dark">NOM Prénom</a></p>
                            </li>
                            <li class="row text-center justify-content-center">
                                <img class="float-left m-2" style="width: 70px;" alt="Image de profil" src="https://cdn-images.zoobeauval.com/JeXvZ0A8GGle5C36g86r6Cm143c=/1000x750/https://s3.eu-west-3.amazonaws.com/images.zoobeauval.com/2020/06/bn-1-5ef05b46dec6b.jpg">
                                <p class="lead align-items-center pt-2"><a class="text-dark">NOM Prénom</a></p>
                            </li>
                            <li class="row text-center justify-content-center">
                                <img class="float-left  m-2 " style="width: 70px;" alt="Image de profil" src="https://cdn-images.zoobeauval.com/JeXvZ0A8GGle5C36g86r6Cm143c=/1000x750/https://s3.eu-west-3.amazonaws.com/images.zoobeauval.com/2020/06/bn-1-5ef05b46dec6b.jpg">
                                <p class="lead align-items-center pt-2"><a class="text-dark">NOM Prénom</a></p>
                            </li>
                            <li class="row text-center justify-content-center">
                                <img class="float-left m-2" style="width: 70px;" alt="Image de profil" src="https://cdn-images.zoobeauval.com/JeXvZ0A8GGle5C36g86r6Cm143c=/1000x750/https://s3.eu-west-3.amazonaws.com/images.zoobeauval.com/2020/06/bn-1-5ef05b46dec6b.jpg">
                                <p class="lead align-items-center pt-2"><a class="text-dark">NOM Prénom</a></p>
                            </li>
                            <li class="row text-center justify-content-center">
                                <img class="float-left m-2" style="width: 70px;" alt="Image de profil" src="https://cdn-images.zoobeauval.com/JeXvZ0A8GGle5C36g86r6Cm143c=/1000x750/https://s3.eu-west-3.amazonaws.com/images.zoobeauval.com/2020/06/bn-1-5ef05b46dec6b.jpg">
                                <p class="lead align-items-center pt-2"><a class="text-dark">NOM Prénom</a></p>
                            </li>
                        </ul>
                    </section>
                    <section class="col-md-4 offset-md-2 border border-primary overflow-auto" style="height: 450px; overflow-y: scroll;">
                        <h1 class="display-5 text-center">Suggestions</h1>
                        <ul class="list-unstyled">
                            {*{for $foo=1 to 5}
                                <div class="row text-center justify-content-center">
                                    <div class="col-4">
                                        <img class="float-left m-2" style="width: 70px;" alt="Image de profil"  src={$suggestion[$foo]['imageUser']}>
                                    </div>
                                    <form method="post" action="profil" class="col-6">
                                        <h6 name="nom">{$suggestion[$foo]['name']}</h6>
                                        <button name="btn" type="submit" class="btn btn-primary">Voir le profil</button>
                                    </form>
                                </div>
                            {/for}*}
                            <form action="profil" method="post">
                                <div class="row text-center justify-content-center">
                                    <div class="col-4">
                                        <img class="float-left m-2" style="width: 70px;" alt="Image de profil"  src={$suggestion[1]['imageUser']}>
                                    </div>
                                    <div class="col-6">
                                        <button name="btn" type="submit" class="btn text-dark" value="{$suggestion[1]['emailUser']}">{$suggestion[1]['name']}</button>
                                    </div>
                                </div>
                                <div class="row text-center justify-content-center">
                                    <div class="col-4">
                                        <img class="float-left m-2" style="width: 70px;" alt="Image de profil"  src={$suggestion[1]['imageUser']}>
                                    </div>
                                    <div class="col-6">
                                        <button name="btn" type="submit" class="btn text-dark" value="{$suggestion[2]['emailUser']}">{$suggestion[2]['name']}</button>
                                    </div>
                                </div>
                                <div class="row text-center justify-content-center">
                                    <div class="col-4">
                                        <img class="float-left m-2" style="width: 70px;" alt="Image de profil"  src={$suggestion[3]['imageUser']}>
                                    </div>
                                    <div class="col-6">
                                        <button name="btn" type="submit" class="btn text-dark" value="{$suggestion[2]['emailUser']}">{$suggestion[3]['name']}</button>
                                    </div>
                                </div>
                            </form>
                        </ul>
                    </section>
                </section>
            </div>
        </div>
    </body>
</html>