{*Commentaire Smarty*}
<!doctype html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>{$titre}</title>
    </head>
		<body>
	<div class="container">
			<nav class="navbar navbar-dark bg-primary">
				<a class="navbar-brand" href="/">Social-Network</a>
					<a href="login" class="btn btn-link text-white border-left rounded-0 ml-3" >Se connecter</a>
			</nav>
			<section class="my-5">
				<div class="text-center">
					<h1>S'inscrire</h1>
				</div>
			</section>

			<form class="w-75 container" method="post" action="register">
				<section class="my-3">
				<div class="row">
					<div class="col-6 col-md-4 text-center">
						<img src={$img['image']|default:'https://via.placeholder.com/220'} class="img-thumbnail rounded" alt="image de profil"/>               
					</div>
					<div class="col-12 col-sm-6 col-md-8">
					<div class="jumbotron py-5">
						<h2 for="image" class="pb-2">Image de profil * </h2>
						<input type="text" name="image" class="form-control mb-4" placeholder={$img['image']|default:'https://via.placeholder.com/150'}>
						<span class="text-danger">{$messages['image']}</span>
						<div class="row">
							<div class="col-4 form-inline">
								<button name="previsualiser" type="submit" class="btn btn-outline-primary">Prévisualiser</button>
							</div>	
						</div>
					</div>
				</div>
				</section>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 ">
    						<label for="firstname">Prénom* :</label>
						</div>
						<div class="form-group col-md-6">
							<input type="text" id="firstname" class="form-control" name="prenom" value="{$retour['prenom']|default:''}" placeholder="Saisir prénom">
        					<span class="text-danger">{$messages['nom']|default:''}</span>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="lastname">Nom* : </label>
						</div>
						<div class="form-group col-md-6">
							<input name="nom" type="text" class="form-control" id="lastname" placeholder="Saisir nom" value="{$retour['nom']|default:''}">
							<span class="text-danger">{$messages['prenom']|default:''}</span>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="inputState">Genre* : </label>
						</div>
						<div class="form-group col-md-6">
							<select name="genre" id="inputState" class="form-control">
								<option selected>Sélectionner...</option>
								<option>Homme</option>
								<option>Femme</option>
								<option>Autre</option>
							</select>
							<span class="text-danger">{$messages['genre']|default:''}</span>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="inputState">Adresse email* : </label>
						</div>
						<div class="form-group col-md-6">
							<input name="mail" type="email" class="form-control" id="mail" placeholder="Saisir email" value="{$retour['mail']|default:''}" >
						    <span class="text-danger">{$messages['mail']|default:''}</span>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="pwdVerif">Mot de passe* : </label>
						</div>
						<div class="form-group col-md-6">
							<input name="motdepasse" type="password" class="form-control" id="motdepasse" placeholder="Saisir mot de passe" >
							<span class="text-danger">{$messages['motdepasse']|default:''}</span>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="pwdVerif">Mot de passe (confirmation)* : </label>
						</div>
						<div class="form-group col-md-6">
							<input name="motdepasseverif" type="password" class="form-control" id="pwdVerif" placeholder="Saisir mot de passe (confirmation)" >
							<span class="text-danger">{$messages['motdepasseverif']|default:''}</span>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="dateNaissance">Date de naissance* : </label>
						</div>
						<div class="form-group col-md-6">
							<input name="birth" type="date" min="1900-01-01" class="form-control" id="dateNaissance" placeholder="Date de naissance" value="{$retour['birth']|default:''}">
							<span class="text-danger">{$messages['birth']|default:''}</span>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="dernierDiplome">Dernier diplôme* : </label>
						</div>
						<div class="form-group col-md-6">
							<input name="degree" type="text" class="form-control" id="dernierDiplome" placeholder="Dernier diplôme" value="{$retour['degree']|default:''}">
							<span class="text-danger">{$messages['degree']|default:''}</span>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="villeRes">Ville de résidence* : </label>
						</div>
						<div class="form-group col-md-6">
							<input name="city" type="text" class="form-control" id="villeRes" placeholder="Ville de résidence" value="{$retour['city']|default:''}" >
							<span class="text-danger">{$messages['city']|default:''}</span>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="textArea">Description : </label>
						</div>
						<div class="form-group col-md-6">
							<textarea name="description" class="form-control" id="textArea" rows="3">{$retour['description']|default:''}</textarea>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<label for="textArea">Status : </label>
						</div>
						<div class="form-group col-md-6">
							<textarea name="status" class="form-control" id="textArea" rows="2">{$retour['status']|default:''}</textarea>
						</div>
					</div>
					<div class="form-row justify-content-center">
						<div class="form-group col-md-4 py-0">
							<p>*: champs obligatoire</p>
						</div>
					<div class="form-group col-md-6">
							<button name="subscribe" type="submit" class="btn btn-primary btn-lg">S'inscrire</button>
						</div>
					</div>							
				</form>
		</body> 
</html>