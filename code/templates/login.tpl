{*Commentaire Smarty*}
<!doctype html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>{$titre}</title>
    </head>
    <body>
		<div class="container">
			<nav class="navbar navbar-dark bg-primary">
				<a class="navbar-brand" href="/">Social-Network</a>
				<a href="/register" class="btn btn-link text-white">S'inscrire</a>
			</nav>
		<form class="w-75 container" method="post" action="login">
			<section class="text-center mt-3">
				<h1>Se connecter</h1>
			</section>
			<section class="border border-primary bg-light rounded col-md-6 offset-md-3 mt-5 pt-5 pb-5">
            	<div class="form-row justify-content-center">
                    <div class="form-group col-md-4 ">
						<label for="email">Adresse email* : </label>
					</div>
					<div class="form-group col-md-6">
						<input type="email" name="email" class="form-control" id="email" placeholder="Saisir l'adresse email">
						<span class="text-danger">{$messages['email']|default:''}</span>
					</div>
					<div class="form-group col-md-4 ">
                        <label for="pwd">Mot de passe* : </label>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="password" name="motdepasse" class="form-control" id="pwd" placeholder="Saisir le mot de passe">
						<span class="text-danger">{$messages['motdepasse']|default:''}</span>
					</div>
                </div>				
				<div class="row justify-content-center">
					<span class="text-danger d-block">{$messages['mailOrPwd']|default:''}</span>
				</div>
				<div class="row justify-content-center">
					<button type="submit" class="btn btn-primary mt-3">Se connecter</button>
				</div>
			</section>
		</form>
		</div>
    </body>
</html>