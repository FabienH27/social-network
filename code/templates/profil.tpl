{*Commentaire Smarty*}
<!doctype html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>{$titre}</title>
    </head>
    <body>
	<div class="container">

        <nav class="navbar navbar-dark bg-primary">
            <a href="./" class="navbar-brand" href="">Social-Network</a>
                <div class="col-6 text-right">
                {if isset($connecte)}
                    <a href="./logout" class="btn btn-primary btn-lg">Se déconnecter</a>
                {else}
                    <a href="./register" class="btn btn-primary btn-lg">S'inscrire</a>
                    <a href="./login" class="btn btn-primary btn-lg ml-3">Se connecter</a>
                {/if}
                </div>
        </nav>

	  <section class="my-3">
		<div class="row">
		  <div class="col-6 col-md-4 text-center">
			  <img class="img-fluid" src="{$donnees['imageUser']|default:"https://via.placeholder.com/220"}" alt="logo"/>               
		  </div>
		  <div class="col-12 col-sm-6 col-md-8">
			<div class="jumbotron">
			  <h1>{$donnees['firstNameUser']}<span class="text-primary"> {$donnees['lastNameUser']}</span></h1>
			  <p class="lead ml-5">{$donnees['statusUser']|default:"Aucun statut."}</p>
			</div>
		  </div>
		</div>
	  </section>

	  <section class="my-3">
		<div class="row">
			<div class="col-4">
				<div>
					{if isset($session['connecte']) && $donnees['nom'] !== $session['nom']}
					<div class="card card-body text-center btn btn-outline-primary">
						<h5 class="card-title align-middle m-0">Devenir amis</h5>
					</div>
					{/if}
				</div>
		  </div>        
		  <div class="col-8">
			<div clas="card-title">
			  <div class="card-header text-center ">
				<p>{$donnees['descriptionUser']|default:"Aucune description."}</p>
			  </div>
			</div>
		  </div>
		</div>
	  </section>

	  <section class="my-3">
		<div class="row">
		  <div class="col-4">
			<div class="card p-4">
				<p><span class="text-primary">Nom</span> : {$donnees['lastNameUser']}</p>
				<p><span class="text-primary">Prénom</span> : {$donnees['firstNameUser']}</p>
				<p><span class="text-primary">Genre</span> : {$donnees['genderUser']}</p>
				<p><span class="text-primary">Age</span> : {$donnees['age']}</p>
				<p><span class="text-primary">Date d'inscription</span> : jj/mm/aaaa</p>
				<p><span class="text-primary">Dernier diplôme</span> : {$donnees['degreeUser']}</p>
				<p><span class="text-primary">Ville de résidence</span> : {$donnees['cityUser']}</p>
				<p><span class="text-primary">Nombre d'amis</span>: XX</p>
			</div>
		  </div>
		  <div class="col-8 px-0 overflow-auto">
			<div class="overflow-auto" style="height: 400px; overflow-y: scroll;">
			  <div class="card">
				<div class="card-body">
				  <p class="card-text inline float-right"><small class="text-muted">{$donnees['']}</small></p>
				  <h5 class="card-title">Prénom Nom</h5>
				  <p class="card-text">Ceci est un message posté par un utilisateur. Ceci est un message posté par un utilisateur.</p>
				  <button class="float-right btn inline">
					<svg width="2em" viewBox="0 0 16 16" class="bi bi-person-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zm1.146-7.85a.5.5 0 0 1 .708 0L14 6.293l1.146-1.147a.5.5 0 0 1 .708.708L14.707 7l1.147 1.146a.5.5 0 0 1-.708.708L14 7.707l-1.146 1.147a.5.5 0 0 1-.708-.708L13.293 7l-1.147-1.146a.5.5 0 0 1 0-.708z"/>
					</svg>
				  </button>
				</div>
			  </div>
			  {*<div class="card">
				<div class="card-body">
				  <p class="card-text inline float-right"><small class="text-muted">Le jj/mm/aaaa</small></p>
				  <h5 class="card-title">Prénom Nom</h5>
				  <p class="card-text">Ceci est un message posté par un utilisateur. Ceci est un message posté par un utilisateur.</p>
				  <button class="float-right btn inline">
					<svg width="2em" viewBox="0 0 16 16" class="bi bi-person-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zm1.146-7.85a.5.5 0 0 1 .708 0L14 6.293l1.146-1.147a.5.5 0 0 1 .708.708L14.707 7l1.147 1.146a.5.5 0 0 1-.708.708L14 7.707l-1.146 1.147a.5.5 0 0 1-.708-.708L13.293 7l-1.147-1.146a.5.5 0 0 1 0-.708z"/>
					</svg>
				  </button>
				</div>
			  </div>
			  <div class="card">
				<div class="card-body">
				  <p class="card-text inline float-right"><small class="text-muted">Le jj/mm/aaaa</small></p>
				  <h5 class="card-title">Prénom Nom</h5>
				  <p class="card-text">Ceci est un message posté par un utilisateur. Ceci est un message posté par un utilisateur.</p>
				  <button class="float-right btn inline">
					<svg width="2em" viewBox="0 0 16 16" class="bi bi-person-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zm1.146-7.85a.5.5 0 0 1 .708 0L14 6.293l1.146-1.147a.5.5 0 0 1 .708.708L14.707 7l1.147 1.146a.5.5 0 0 1-.708.708L14 7.707l-1.146 1.147a.5.5 0 0 1-.708-.708L13.293 7l-1.147-1.146a.5.5 0 0 1 0-.708z"/>
					</svg>
				  </button>
				</div>
			  </div>
			  <div class="card">
				<div class="card-body">
				  <p class="card-text inline float-right"><small class="text-muted">Le jj/mm/aaaa</small></p>
				  <h5 class="card-title">Prénom Nom</h5>
				  <p class="card-text">Ceci est un message posté par un utilisateur. Ceci est un message posté par un utilisateur.</p>
				  <button class="float-right btn inline">
					<svg width="2em" viewBox="0 0 16 16" class="bi bi-person-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zm1.146-7.85a.5.5 0 0 1 .708 0L14 6.293l1.146-1.147a.5.5 0 0 1 .708.708L14.707 7l1.147 1.146a.5.5 0 0 1-.708.708L14 7.707l-1.146 1.147a.5.5 0 0 1-.708-.708L13.293 7l-1.147-1.146a.5.5 0 0 1 0-.708z"/>
					</svg>
				  </button>
				</div>
			  </div>
			  <div class="card">
				<div class="card-body">
				  <p class="card-text inline float-right"><small class="text-muted">Le jj/mm/aaaa</small></p>
				  <h5 class="card-title">Prénom Nom</h5>
				  <p class="card-text">Ceci est un message posté par un utilisateur. Ceci est un message posté par un utilisateur.</p>
				  <button class="float-right btn inline">
					<svg width="2em" viewBox="0 0 16 16" class="bi bi-person-x" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					  <path fill-rule="evenodd" d="M8 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H1s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10zm1.146-7.85a.5.5 0 0 1 .708 0L14 6.293l1.146-1.147a.5.5 0 0 1 .708.708L14.707 7l1.147 1.146a.5.5 0 0 1-.708.708L14 7.707l-1.146 1.147a.5.5 0 0 1-.708-.708L13.293 7l-1.147-1.146a.5.5 0 0 1 0-.708z"/>
					</svg>
				  </button>
				</div>
			  </div>  
			</div>*}
			<form action="profil" method="post">
			  <div class="card p-2">
				<div class="form-group">
					<label for="ZoneTexte">Poster un message</label>
					<textarea name="comment" class="form-control" id="ZoneTexte" rows="3"></textarea>
					<span class="text-danger">{$message['comment']|default:''}</span>
				</div>
				<div class="">
				  <button name="submitComment" type="submit" class="btn btn-primary btn-lg float-right">Poster ce message</button>
				</div>
			  </div>
			</form>
		  </div>
		</div>
	  </section>
	</div>
    </body>
</html>