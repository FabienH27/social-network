{*Commentaire Smarty*}
<!doctype html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>{$titre}</title>
    </head>
    <body>
        <div class="container">
            <nav class="navbar navbar-dark bg-primary">
                <a href="./" class="navbar-brand" href="">Social-Network</a>
                <form class="form-inline">
                    <h5 class="text-white m-0">{$session['nom']}</h5>
                    <a href="/logout" class="btn btn-link text-white border-left rounded-0 ml-3" type="submit">Se déconnecter</a>
                </form>
            </nav>
            <section class="my-5">
                <div class="text-center">
                    <h1>Modifier mes informations</h1>
                </div>
            </section>
            <form action="parametres" method="post">
            <section class="w-75 container">
                    <section class="my-3">
                        <div class="row">
                            <div class="col-6 col-md-4 text-center">
                                <img class="img-fluid" src="{$session['imageUser']}" alt="logo"/>               
                            </div>
                            <div class="col-12 col-sm-6 col-md-8">
                            <div class="jumbotron">
                                <input type="email" class="form-control mb-4" placeholder="{$session['imageUser']}">
                                <div class="row">
                                    <div class="col-4 form-inline">
                                        <button type="button" class=" btn btn-outline-primary">Prévisualiser</button>
                                    </div>
                                    <div class="col-8">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                                            <label class="custom-file-label" for="validatedCustomFile">Choisir un fichier...</label>
                                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-4 ">
                                <label for="email">Email : </label>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control" id="email" value="{$session['emailUser']}">
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-4 py-0">
                                <label for="pwd">Mot de passe : </label>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="password" class="form-control" id="pwd" placeholder="Saisir mot de passe">
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-4 py-0">
                                <label for="pwdVerif">Mot de passe (Vérification) : </label>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="password" class="form-control" id="pwdVerif" placeholder="Saisir mot de passe (Vérification)">
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-4 py-0">
                                <label for="inputState">Genre : </label>
                            </div>
                            <div class="form-group col-md-6">
                                <select id="inputState" class="form-control">
                                    <option selected>Sélectionner...</option>
                                    <option>Homme</option>
                                    <option>Femme</option>
                                    <option>Autre</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-4 py-0">
                                <label for="dateNaissance">Date de naissance : </label>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="date" class="form-control" id="dateNaissance" value="{$session['birthUser']}">
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-4 py-0">
                                <label for="dernierDiplome">Dernier diplôme : </label>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="dernierDiplome" value="{$session['degreeUser']|default:"aucune diplôme renseigné pour le moment."}">
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-4 py-0">
                                <label for="villeRes">Ville de résidence : </label>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control" id="villeRes" value="{$session['cityUser']|default:"aucune ville renseignée pour le moment."}">
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-4 py-0">
                                <label for="textArea">Description : </label>
                            </div>
                            <div class="form-group col-md-6">
                                <textarea class="form-control" id="textArea" rows="3">{$session['descriptionUser']|default:"aucune description."}</textarea>
                            </div>
                        </div>
                        <div class="form-row justify-content-center">
                            <div class="form-group col-md-4 py-0">
                                <label for="statut">Statut personnalisé : </label>
                            </div>
                            <div class="form-group col-md-6">
                                <textarea class="form-control" id="statut" rows="2">{$session['statusUser']|default:"aucun statut."}</textarea>
                            </div>
                        </div>
                        <div class="form-row text-center my-5">
                            <div class="form-group col-md-6 ">
                                <button type="submit" class="btn btn-primary btn-lg">Enregistrer</button>
                            </div>
                            <div class="form-group col-md-6">
                                <button type="submit" class="btn btn-outline-primary btn-lg">Retour au profil</button>
                            </div>
                        </div>
                    </section>
                </section>
            </form>
        </div>
    </body>
</html>