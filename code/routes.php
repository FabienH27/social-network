<?php
/**
 * PHP Version 7.3
 *
 * @category Routes
 * @package  Routes
 * @author   fabienh <fhannon60@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     routes.php
 * Definitions of project's routes.
 */

    include_once './src/functions.php';

    Flight::route(
        'GET /register', function () {
            Flight::render(
                "templates/register.tpl",
                array("titre"=>"Social Network - S'inscrire","img"=>$_GET)
            );
        }
    );

    Flight::route(
        'POST /register', function () {
            $db = Flight::get('db');
            $messages=array();

            $tempImg = $_POST['image'];

            if (isset($_POST['previsualiser'])) {
                if (empty($_POST['image'])) {
                    $messages['image'] = "l'image est obligatoire";
                }
                Flight::view()->display(
                    'register.tpl',
                    array("titre"=>"Register", "messages"=>$messages,"img"=>$_POST)
                );
            }
            if (isset($_POST['subscribe'])) {
                // test nom
                if (empty($_POST['nom'])) {
                    $messages['nom'] = "Le nom est obligatoire";
                }

                $_POST['image'] = "https://via.placeholder.com/220";
                //test prenom
                if (empty($_POST['prenom'])) {
                    $messages['prenom'] = "Le prenom est obligatoire";
                }
                //test genre
                if ($_POST['genre'] == "Sélectionner...") {
                    $messages['genre'] = "Le genre est obligatoire";
                }
                //test mail
                if (empty($_POST['mail'])) {
                    $messages['mail'] = "L'adresse mail est obligatoire";
                } elseif (filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL) === false) {
                    $messages['mail'] = "L'email est invalide";
                } else {
                    $st=$db->prepare(
                        "select emailUser from Users 
                        where emailUser = ?"
                    );
                    $st->execute(array($_POST['mail']));
                    if ($st->rowCount()>0) {
                        $messages['mail']="L'email existe déjà";
                    }
                }
                //test motdepasse
                if (empty($_POST['motdepasse'])) {
                    $messages['motdepasse'] = "Le mot de passe est obligatoire";
                } elseif (strlen($_POST['motdepasse'])<8) {
                    $messages['motdepasse']="Le mot de passe doit contenir au moins 8 caractères";
                }
                //test verif motdepasse
                if ($_POST['motdepasseverif'] != $_POST['motdepasse']) {
                    $messages['motdepasseverif'] = "Les mot de passes ne correspondent pas";
                }
                //test date naissance
                if (empty($_POST['birth'])) {
                    $messages['birth'] = "La date de naissance est obligatoire";
                }
                //test diplome
                if (empty($_POST['degree'])) {
                    $messages['degree'] = "Le diplome est obligatoire";
                }
                //test ville
                if (empty($_POST['city'])) {
                    $messages['city'] = "La ville est obligatoire";
                }
                if (empty($_POST['image']) && empty($tempImg)) {
                    $messages['image'] = "L'image est obligatoire";
                }
                if (empty($messages)) {
                    $req = $db->prepare(
                        "INSERT INTO `Users`(`firstNameUser`,`lastNameUser`,`emailUser`,
                        `pwdUser`,`imageUser`,
                        `genderUser`,`birthUser`,
                        `degreeUser`,`cityUser`,
                        `descriptionUser`)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
                    );
                    $req->execute(
                        array($_POST['prenom'],
                        $_POST['nom'],
                        $_POST['mail'],
                        password_hash($_POST['motdepasse'], PASSWORD_DEFAULT),
                        $_POST['image'],$_POST['genre'],
                        $_POST['birth'],$_POST['degree'],
                        $_POST['city'],$_POST['description'])
                    );
                    Flight::redirect('/');
                } else {
                    Flight::view()->display('register.tpl', array("titre"=>"Register", "messages"=>$messages,"retour"=>$_POST));
                }
            }
        }
    );

    Flight::route(
        'GET /login', function () {
            Flight::render("templates/login.tpl", array("titre"=>"Social Network - Se connecter"));
        }
    );

    Flight::route(
        'POST /login', function () {
            $messages = false;
            $db = Flight::get('db');
            if (empty($_POST['email'])) {
                $messages['email']="L'email est obligatoire";
            } else if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)===false) {
                $messages['email']="L'email est invalide";
            } else {
                $st=$db->prepare("select emailUser from Users where emailUser = ?");
                $st->execute(array($_POST['email']));
                if ($st->rowCount() < 1) {
                    $messages['email']="L'email n'existe pas dans la table";
                }
            }
            if (empty($_POST['motdepasse'])) {
                $messages['motdepasse'] = "Le mot de passe est obligatoire";
            }

            if (empty($messages)) {
                $st=$db->prepare("select * from Users where emailUser = ?");
                $st->execute(array($_POST['email']));
                if ($data = $st->fetch()) {
                    if (password_verify($_POST['motdepasse'], $data['pwdUser'])) {
                        $_SESSION['logged'] = true;
                        $_SESSION['nom'] = $data['firstNameUser']." ".$data['lastNameUser'];
                        $_SESSION['idUser'] = $data['idUser'];
                        $_SESSION['firstNameUser'] = $data['firstNameUser'];
                        $_SESSION['lastNameUser'] = $data['lastNameUser'];
                        $_SESSION['emailUser'] = $data['emailUser'];
                        $_SESSION['imageUser'] = $data['imageUser'];
                        $_SESSION['genderUser'] = $data['genderUser'];
                        $_SESSION['birthUser'] = $data['birthUser'];
                        $_SESSION['degreeUser'] = $data['degreeUser'];
                        $_SESSION['cityUser'] = $data['cityUser'];
                        $_SESSION['descriptionUser'] = $data['descriptionUser'];
                        $_SESSION['statusUser'] = $data['statusUser'];
                    } else {
                        $messages['mailOrPwd'] = "Adresse email ou mot de passe invalide";
                    }
                } else {
                    $messages['mailOrPwd'] = "Adresse email ou mot de passe invalide";
                }
            }
            if ($messages) {
                //print_r($messages);
                Flight::render("templates/login.tpl", array("titre"=>"Social Network - Se connecter", "post" => $_POST, "messages" => $messages));
            } else {
                Flight::redirect("/");
            }
        }
    );

    Flight::route(
        '/', function () {
            $db = Flight::get('db');
            $suggestion = array();
            for ($i = 1; $i <= 10; $i++){
                $randomID = rand(1, 10);
                $st=$db->prepare("select idUser, firstNameUser, lastNameUser, emailUser, imageUser from Users where idUser = ?");
                $st->execute(array($randomID));
                $data[$i] = $st->fetch();
                $suggestion[$i] = $data[$i];
                $suggestion[$i]['name'] = $data[$i]['firstNameUser']." ".$data[$i]['lastNameUser'];
            }
            $result = array_unique($suggestion, SORT_REGULAR);
            
            Flight::render(
                "templates/index.tpl", array("titre"=>"Social Network - Home",
                "connecte"=>(isset($_SESSION['logged']) && $_SESSION['logged'] ? $_SESSION['logged'] : null),
                "nom" => (isset($_SESSION['nom']) ? $_SESSION['nom'] : false),"suggestion"=>$result)
            );
        }
    );

    Flight::route(
        'POST /profil', function () {
            $db = Flight::get('db');
            $data = array();
            $message = array();
            if (isset($_POST['submitComment'])) {
                $date = date("Y-m-d");
                $req = $db->prepare("INSERT INTO `Comments`(`idAuthorComment`,`contentComment`,`dateComment`) VALUES (?, ?, ?);");
                $req->execute(array($_SESSION['idUser'],$_POST['comment'],$date));
                if (empty($_POST['comment'])) {
                    $message['comment'] = "Merci d'écrire un commentaire valide.";
                }
                Flight::render(
                    "templates/profil.tpl", array("titre"=>"Social Network - Profil",
                    "donnees"=>$data,"connecte"=>$_SESSION['connecte'],
                    "message"=>$message)
                );
            }else if(isset($_POST['myProfile'])) {
                $st=$db->prepare(
                    "select firstNameUser, 
                    lastNameUser, emailuser, 
                    imageUser, genderUser, 
                    birthUser, degreeUser, 
                    cityUser, descriptionUser, 
                    statusUser from Users where idUser = ?"
                );
                $st->execute(array($_SESSION['idUser']));
                $data = $st->fetch();
                $data['nom'] = $data['firstNameUser']." ".$data['lastNameUser'];
                Flight::render(
                    "templates/profil.tpl",
                    array("titre"=>"Social Network - Profil",
                    "donnees"=>$data,
                    "session"=>$_SESSION,
                    "message"=>$message)
                );
            }else {
                $st=$db->prepare(
                    "select firstNameUser, 
                    lastNameUser, emailuser, 
                    imageUser, genderUser, 
                    birthUser, degreeUser, 
                    cityUser, descriptionUser, 
                    statusUser from Users where emailUser = ?"
                );
                $st->execute(array($_POST['btn']));
                $data = $st->fetch();
                $data['nom'] = $data['firstNameUser']." ".$data['lastNameUser'];
                $age = calculAge($data['birthUser']);
                $data['age'] = $age;
                Flight::render(
                    "templates/profil.tpl",
                    array("titre"=>"Social Network - Profil",
                    "donnees"=>$data,
                    "session"=>$_SESSION,
                    "message"=>$message)
                );
            }
        }
    );

    Flight::route(
        'GET /parametres', function () {
            Flight::render(
                "templates/parametres.tpl",
                array("titre"=>"Social Network - Paramètres", "session"=>$_SESSION)
            );
        }
    );

    Flight::route(
        'POST /parametres', function () {
            $db = Flight::get('db');
            $data = array();
            $req = $db->prepare(
                "INSERT INTO `Users`(`firstNameUser`,`lastNameUser`,`emailUser`,
            `pwdUser`,`imageUser`,
            `genderUser`,`birthUser`,
            `degreeUser`,`cityUser`,
            `descriptionUser`)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
            );
            $req->execute(
                array($_POST['prenom'],
                $_POST['nom'],
                $_POST['mail'],
                password_hash($_POST['motdepasse'], PASSWORD_DEFAULT),
                $_POST['image'],$_POST['genre'],
                $_POST['birth'],$_POST['degree'],
                $_POST['city'],$_POST['description'])
            );
            
        }
    );

    Flight::route(
        '/logout', function () {
            $_SESSION = array();
            session_destroy();
            session_start();
            $_SESSION['logged'] = false;
            Flight::redirect("/");
        }
    );

    ?>