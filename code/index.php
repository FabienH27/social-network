<?php

/**
 * PHP Version 7.3
 *
 * @category Index
 * @package  Index
 * @author   fabienh <fhannon60@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     Index.php
 * Starting frameworks, database and project.
 */

session_start();
require 'vendor/autoload.php';
require "./includes/pdo.php";

require 'routes.php';

Flight::register(
    'view', 'Smarty', array(), function ($smarty) {
        $smarty->template_dir = './templates/';
        $smarty->compile_dir = './templates_c/';
        $smarty->config_dir = './config/';
        $smarty->cache_dir = './cache/';
    }
);
Flight::map(
    'render', function ($template, $data) {
        Flight::view()->assign($data);
        Flight::view()->display($template);
    }
);

Flight::set('db', $db);

Flight::start();
?>
