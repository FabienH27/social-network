# Social-Network
## Projet M3301

Création d'un petit réseau social en utilisant Docker, composer et PHP.

Installation du dépôt : 
- Clone du dépôt
- Démarrer le container : docker-compose up
- Entrée dans le container : docker-compose exec php /bin/bash
- Installation de composer : /code# composer install

___

Auteurs :
Benoit CHEVILLON
Fabien HANNON
Lucas PHILIPPE
